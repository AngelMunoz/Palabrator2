import { Aurelia } from 'aurelia-framework'
import { PLATFORM } from 'aurelia-pal';
import { I18N, Backend, TCustomAttribute } from 'aurelia-i18n';
import UIkit from 'uikit';
import environment from './environment';
import Icons from 'uikit/dist/js/uikit-icons';
import 'intl';
import 'uikit/dist/css/uikit.min.css';
import './main.css';
UIkit.use(Icons);

export function configure(aurelia: Aurelia) {
  aurelia.use
    .standardConfiguration()
    .feature(PLATFORM.moduleName('resources/index'))
    .plugin(PLATFORM.moduleName('aurelia-i18n'), (instance: I18N) => {
      let aliases = ['t', 'i18n'];
      // add aliases for 't' attribute
      TCustomAttribute.configureAliases(aliases);
      // register backend plugin
      instance.i18next.use(Backend.with(aurelia.loader));
      return instance.setup({
        backend: {                                  // <-- configure backend settings
          loadPath: 'locales/{{lng}}/{{ns}}.json', // <-- XHR settings for where to get the files from
        },
        skipTranslationOnMissingKey: true,
        attributes: aliases,
        lng: 'es',
        fallbackLng: 'en',
        debug: environment.debug
      });
    });

  aurelia.use.developmentLogging(environment.debug ? 'debug' : 'warn');

  if (environment.testing) {
    aurelia.use.plugin(PLATFORM.moduleName('aurelia-testing'));
  }

  aurelia.start().then(() => aurelia.setRoot(PLATFORM.moduleName('app')));
}
