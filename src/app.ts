import { autoinject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { I18N } from 'aurelia-i18n';
import { RouterConfiguration, Router } from 'aurelia-router';
import { AuthService } from 'fire-services';
import { getRoutes } from 'routes';
import environment from 'environment';
import UIkit from 'uikit';
import { AuthEvents } from 'enums';

@autoinject
export class App {
  private router: Router;
  private offcanvasRef: HTMLElement;
  private offcanvas: any;


  constructor(
    private i18n: I18N,
    private auth: AuthService,
    private ea: EventAggregator
  ) {

    if (!environment.debug) {
      const en = navigator.language.includes('en');
      const locale = localStorage.getItem('chosenLocale') ? localStorage.getItem('chosenLocale') : en ? 'en' : 'es';
      i18n.setLocale(locale);
    }

    this.ea.subscribe(AuthEvents.AUTH_STATE_CHANGED, user => user ? this.router.navigate('/profiles') : undefined);
  }

  configureRouter(config: RouterConfiguration, router: Router): void {
    this.router = router;
    config.options.pushState = true;
    config.title = this.i18n.tr('app.title');
    config.map(getRoutes(this.i18n));
  }

  protected attached() {
    this.offcanvas = UIkit.offcanvas(this.offcanvasRef, { mode: 'push', 'esc-close': true });
  }

  public get authenticated() {
    return this.auth.authenticated;
  }

  public async offCanvasToggle() {
    await this.offcanvas.toggle();
  }
}
