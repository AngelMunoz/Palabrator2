export enum AuthEvents {
  AUTH_STATE_CHANGED = 'on:auth-state:changed'
}
