import { EventAggregator } from 'aurelia-event-aggregator';
import { autoinject, singleton } from 'aurelia-framework';
import { FireApp } from '.';
import { AuthPayload } from 'interfaces';
import { AuthEvents } from 'enums';

@autoinject
export class AuthService {
  private readonly auth: firebase.auth.Auth;

  constructor(private ea: EventAggregator) {
    this.auth = FireApp.auth();
    this.auth.onAuthStateChanged(user => this.ea.publish(AuthEvents.AUTH_STATE_CHANGED, user));
  }

  get userId() {
    return this.auth.currentUser ? this.auth.currentUser.uid : undefined;
  }

  get currentUser() {
    return this.auth.currentUser;
  }

  get authenticated() {
    return !!this.auth.currentUser
  }

  public signin({ email, password }: AuthPayload) {
    return this.auth.signInWithEmailAndPassword(email, password);
  }

  public signup({ email, password }: AuthPayload) {
    return this.auth.createUserWithEmailAndPassword(email, password)
  }

  public resetPassword(email: string) {
    return this.auth.sendPasswordResetEmail(email);
  }
}