import 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import { initializeApp } from 'firebase/app';
import environment from 'environment';

export const FireApp = initializeApp(environment.firebase);

export * from './auth.service';