import UIKit from 'uikit';
import { INotificationOptions } from 'interfaces';


export function notify(options: INotificationOptions) {
  UIKit.notification(options);
}

export function notifyError(message: string, options: Partial<INotificationOptions> = {}) {
  const opts: INotificationOptions = {
    ...options,
    message,
    status: 'danger'
  };
  notify(opts);
}

export function notifySuccess(message: string, options: Partial<INotificationOptions> = {}) {
  const opts: INotificationOptions = {
    ...options,
    message,
    status: 'success'
  };
  notify(opts);
}

export function notifyWarning(message: string, options: Partial<INotificationOptions> = {}) {
  const opts: INotificationOptions = {
    ...options,
    message,
    status: 'warning'
  };
  notify(opts);
}

export function notifyPrimary(message: string, options: Partial<INotificationOptions> = {}) {
  const opts: INotificationOptions = {
    ...options,
    message,
    status: 'primary'
  };
  notify(opts);
}