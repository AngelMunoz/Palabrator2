import { AuthService } from "fire-services";
import { autoinject } from 'aurelia-framework';
import { notifyError } from "helpers";

@autoinject
export class ForgotVm {

  email: string = '';
  notSent = true;

  constructor(private auth: AuthService) { }

  get canSubmit() {
    return this.email.length > 4 && this.notSent;
  }

  async resetPassword() {
    try {
      await this.auth.resetPassword(this.email);
    } catch (error) {
      notifyError(error.message);
      return;
    }
    this.notSent = false;
  }
}