import { autoinject, computedFrom } from 'aurelia-framework';
import { AuthService } from 'fire-services';
import { notifyError } from 'helpers/notifications.helper';

@autoinject
export class SignInVm {
  email: string = '';
  password: string = '';
  constructor(private auth: AuthService) { }

  get canSubmit() {
    return this.email.length > 4 && this.password.length > 4;
  }

  async signin() {
    try {
      await this.auth.signin({ email: this.email, password: this.password })
    } catch (error) {
      notifyError(error.message);
      return;
    }
  }
}