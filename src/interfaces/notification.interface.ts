export interface INotificationOptions {
  message: string;
  status: 'primary' | 'success' | 'warning' | 'danger';
  pos?: string;
  timeout?: number;
  group?: string;
}