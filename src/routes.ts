import { PLATFORM } from 'aurelia-framework';
import { RouteConfig } from 'aurelia-router';
import { I18N } from 'aurelia-i18n';

export function getRoutes(i18n: I18N): RouteConfig[] {
  return [
    {
      moduleId: PLATFORM.moduleName('./pages/auth/signin'),
      name: 'signin',
      nav: false, auth: false, route: ['', 'signin'],
      settings: { icon: 'sign-in' },
      title: i18n.tr('signin.title'),
    },
    {
      moduleId: PLATFORM.moduleName('./pages/auth/forgot'),
      name: 'forgot',
      nav: false, auth: false, route: 'forgot',
      settings: { icon: 'user' },
      title: i18n.tr('forgot.title'),
    },
  ];
}