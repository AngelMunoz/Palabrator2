import { bindable, autoinject } from 'aurelia-framework';
import { Router, NavModel } from 'aurelia-router';

@autoinject
export class PlbNavbar {
  @bindable router: Router;
  @bindable authenticated: boolean;

  constructor(private el: Element) { }

  protected onCanvasToggle() {
    this.el.dispatchEvent(new Event('on-canvas-toggle', { bubbles: true, composed: true }));
  }
}
